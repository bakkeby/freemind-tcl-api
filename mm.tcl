#
# Copyright (c) 2014, Stein Gunnar Bakkeby, https://bitbucket.org/bakkeby/freemind-tcl-api
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, are permitted
# provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions
#    and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of
#    conditions and the following disclaimer in the documentation and/or other materials provided
#    with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
# FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
# WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
# 

package require tdom

package provide freemind_api 1.0

namespace eval mm {

	variable MM

	namespace export fold_all
	namespace export unfold_all
	namespace export unfold_leaf_nodes
	namespace export mark_dirty
	namespace export node_id_search
	namespace export node_path_search
	namespace export node_path_create
	namespace export get_linked_node
	namespace export get_root_node
	namespace export sort_subnodes
	namespace export get_dom
	namespace export get_xml
	namespace export load
	namespace export save
	namespace export create_empty_mindmap
}

#
# Procedure to split parameters based on a given prefix.
#
# For example:
#    --type d -i --file ex1.txt ex2.txt --num 1 2 3
#
# would be split up into a list on the form {param} {value...} {param} {value...} ...
#
# I.e.:
#    {--type} {d} {-i} {} {--file} {ex1.txt ex2.txt ex3.txt} {--num} {1 2 3}
#
# @param arguments       List of arguments to split
# @param strip_prefix    Option to remove the prefix from the params in the returned list
# @param arg_prefix      Specifies the prefix used, defaults to hyphen (-)
# @return                A list on the form {param} {value...} {param} {value...} ...
#
proc mm::_split_args { arguments {strip_prefix 0} {arg_prefix -} } {

	set output {}
	set value {}
	foreach arg $arguments {
		# Argument is a param
		if {[regexp ^$arg_prefix+ $arg]} {
			if {[llength $output] > 0} {
				lappend output $value
				set value {}
			}
			if {$strip_prefix} {
				set arg [string trimleft $arg $arg_prefix]
			}
			lappend output $arg
		} else {
			lappend value $arg
		}
	}
	if {[llength $output] > 0} {
		lappend output $value
	}
	return $output
}

#
# Procedure to retrieve the value of a given parameter from a list
# of arguments.
#
# Synopsis:
#    mm::get_arg <list> <parameter> ?<default>? ?<prefix>?
#
# For example:
#    set args [list --type d -i --file ex1.txt ex2.txt --num 1 2 3 --file ex3.txt]
#    set file [mm::get_arg $args --file]
#
# will return:
#    [list ex1.txt ex2.txt ex3.txt]
#
# @param arguments       The list of arguments to search through
# @param param           The parameter to search for
# @param arg_prefix      The parameter prefix, defaults to hyphen (-)
# @param default         The default value if not found
# @return                Empty string if the argument value was not found,
#                        the value if a single value was found, a list of
#                        values if multiple values were found, or "1" if the
#                        argument was found to exist, but with no values.
#
proc mm::get_arg { arguments param {default {}} {arg_prefix -} } {

	set value "\u0"
	set length [llength $arguments]
	set param [string trimleft $param $arg_prefix]
	foreach idx [lsearch -nocase -all -regexp $arguments ^${arg_prefix}+$param\$] {
		while {[incr idx] < $length} {
			set arg [lindex $arguments $idx]
			if {[regexp ^$arg_prefix+\[A-Za-z\] $arg]} {
				break
			}
			if {$value == "\u0"} {
				set value $arg
			} else {
				lappend value $arg
			}
		}
		if {$value == "\u0"} {
			set value 1
		}
	}
	if {$value == "\u0"} {
		set value $default
	}
	return $value
}

#
# Procedure to retrieve the values of multiple from a list of arguments.
#
# Synopsis:
#    mm::get_args <list> <parameters> <defaults>
#
# For example:
#    set args [list --type d -i --file ex1.txt ex2.txt --num 1 2 3 --file ex3.txt]
#    lassign [mm::get_arg $args {--file --type --perm} {default.txt f x}] file type perm
#
# @param arguments       The list of arguments to search through
# @param params          The list of parameters to search for
# @param defaults        The default value should the parameter not exist in
#                        the list of arguments. Expects one default value per
#                        given parameter, if less then will default to empty
#                        string for the remaining parameters.
# @param arg_prefix      The parameter prefix, defaults to hyphen (-)
# @see                   mm::get_arg
# @return                A list of values relative to the list of arguments
#
proc mm::get_args { arguments params {defaults {}} {arg_prefix -} } {

	set output {}
	foreach param $params default $defaults {
		lappend output [mm::get_arg $arguments $param $default $arg_prefix]
	}
	return $output
}

#
# Folds all given nodes and child nodes recursively. If no node is specified then all
# nodes in the mind map are folded.
#
# Leaf nodes will not be marked as folded. The reason for this is that they can not be
# folded manually within FreeMind and if they are marked as folded in the XML then they
# will actually appear as being folded within FreeMind, but cannot be unfolded as they
# have no child nodes.
#
# Synopsis:
#    mm::fold_all ?node? ?node? ... ?
#
# @param node            A node in the mind map whose child nodes should be folded
#
proc mm::fold_all { args } {

	if {[llength $args] == 0} {
		set args [node::get_subnodes [mm::get_root_node]]
	}

	foreach node $args {
		set subnodes [node::get_subnodes $node]
		if {[llength $subnodes] > 0} {
			mm::fold_all {*}$subnodes
			# Leaf node are never folded, hence we only fold
			# if the node has child nodes. If we do fold leaf
			# nodes then they will appear as expandable in
			# FreeMind, but naturally can't be expanded.
			node::fold $node
		}
	}
}

#
# Unfolds all given nodes and child nodes recursively. If no node is specified
# then all nodes in the mind map are unfolded.
#
# Synopsis:
#    mm::unfold_all ?node? ?node? ... ?
#
# @param node            A node in the mind map whose child nodes should be unfolded
#
proc mm::unfold_all { args } {

	if {[llength $args] == 0} {
		set args [node::get_subnodes [mm::get_root_node]]
	}

	foreach node $args {
		set subnodes [node::get_subnodes $node]
		if {[llength $subnodes] > 0} {
			mm::unfold_all {*}$subnodes
		}
		node::unfold $node
	}
}

#
# Unfolds all parent nodes of a given node recursively.
#
# Synopsis:
#   mm::unfold_parents ?node? ?node? ... ?
#
# @param node            The node who's parent nodes should be unfolded
#
proc mm::unfold_parents { args } {

	foreach node $args {
		set parent [$node parentNode]
		if {$parent != {} && [$parent nodeType] == {ELEMENT_NODE}} {
			node::unfold $parent
			mm::unfold_parents $parent
		}
	}
}

#
# Unfolds all leaf nodes of a given node recursively. If no node
# is specified then all leaf nodes in the mind map are unfolded.
#
# As it is possible to create folded nodes it is easy to end up
# with leaf nodes that contains the folded attribute, which within
# FreeMind makes the node look like it has subnodes.
#
# This is a convenience proc that removes the folded attribute from
# all leaf nodes (i.e. nodes that does not have any child nodes).
#
# Synopsis: 
#    mm::unfold_leaf_nodes ?node? ?node? ... ?
#
# @param node            A node in the mind map whose leaf nodes should be unfolded
#
proc mm::unfold_leaf_nodes { args } {

	if {[llength $args] == 0} {
		set args [mm::get_root_node]
	}

	foreach node $args {
		set subnodes [node::get_subnodes $node]
		if {[llength $subnodes] == 0} {
			node::unfold $node
		} else {
			mm::unfold_leaf_nodes {*}$subnodes
		}
	}
}

#
# Mark the mind map as dirty. This is used to indicate that changes
# have been made to the mind map and need to be written to disk.
#
# This can be used to explicitly mark the mind map as dirty if the
# DOM tree has been modified outside of the FreeMind API package.
#
# @see mm::save
#
proc mm::mark_dirty {} {

	variable MM

	set MM(dirty) 1
}

#
# Searches the mind map for a node with a given ID.
#
# Each node in a mind map has a unique ID. This procedure can be used to
# look up a given node if the ID is known.
#
# Synopsis:
#    mm::node_id_search id
#
# @param id              The node ID to search the mind map for
# @return                The node matching the given ID, or empty string
#                        if not found.
#
proc mm::node_id_search { id } {

	variable MM

	set doc [$MM(dom) documentElement]
	return [$doc selectNodes descendant::node\[@ID='$id'\]]
}

#
# Searches the mind map for a node given a specific path.
#
# To look up the "relevant node" a path text search can be performed
# if the ID of the node is not known.
#
# The path should include the text of all nodes in the path, including
# the root node.
#
# Example mind map:
#
#   my mind map
#       +----+path
#              +----+to
#                     +----+relevant node
#
# The path in this case would be "my mind map>path>to>relevant node".
#
# The node separator (defaults to ">") to be used can be specified.
#
# The path search functionality assumes that the node content is plain
# text, i.e. does not contain rich content (html). If a "path" element
# is an asterix then it will match all nodes. Note that a partial match
# such as "mynode>all*>nodes" will not work, but "mynode>*>nodes" will.
# 
# @param path            The path to the relevant node
# @param root_node       The starting point for the path search. If undefined
#                        or empty string then the full path to the node will
#                        have to be provided.
# @param node_separator  Defines how the nodes in the path are separated,
#                        defaults to ">".
# @return                The last node in the path, if found, otherwise
#                        returns empty string. Can return multiple matches.
#
proc mm::node_path_search { path { root_node {} } { node_separator {>} } } {

	variable MM

	if {[string first $node_separator $path] != -1} {
		set split_path [split $path $node_separator]
	} else {
		set split_path [list $path]
	}

	set text [lindex $split_path 0]

	set subnodes [expr {$root_node == {} ? [mm::get_root_node] : $root_node}]
	foreach node_text $split_path {
		set possible_match {}
		if {$node_text == {*}} {
			foreach subnode $subnodes {
				lappend possible_match {*}[node::get_subnodes $subnode]
			}
		} else {
			foreach subnode $subnodes {
				set match [node::get_subnode $subnode $node_text]
				if {$match != {}} {
					lappend possible_match {*}$match
				}
			}
		}
		set subnodes $possible_match
	}

	return $subnodes
}

#
# Similar to path search, but will create any nodes in the path
# if they don't already exist.
#
# @param path            The path to the relevant node
# @param root_node       
# @param node_separator  Defines how the nodes in the path are separated,
#                        defaults to ">".
# @see                   mm::node_path_search
# @return                the last node in the path
#
proc mm::node_path_create { path {root_node {}} {node_separator {>}} {formatting {}} } {

	if {[string first > $path] != -1} {
		set split_path [split $path $node_separator]
	} else {
		set split_path [list $path]
	}

	set parent [expr {$root_node == {} ? [mm::get_root_node] : $root_node}]
	foreach node_text $split_path {
		set subnode [node::get_subnode $parent $node_text]
		if {$subnode == {}} {
			set subnode [node::create --text $node_text --parent $parent {*}$formatting]
		}
		set parent $subnode
	}

	return $parent
}

#
# Sorts the child nodes of a node.
#
# This proc defaults to sort nodes based on text in ascending order. Optionally
# the sorting comparision command can be specified with the second argument and
# this defaults to use mm::compare_node_by_text for this purpose.
# 
# @param node            The node whose child nodes to sort
# @param sort_command    Specify the command to use for sorting the nodes
# @see                   mm::compare_node_by_text
#
proc mm::sort_subnodes { node { sort_command mm::compare_node_by_text } } {

	set node_list [node::get_subnodes $node]
	set sort_list [lsort -command $sort_command $node_list]

	if {$node_list != $sort_list} {
		foreach subnode $sort_list {
			$node removeChild $subnode
			$node appendChild $subnode
		}
		mark_dirty
	}
}

#
# Comparison proc which is can be used with lsort -command.
#
# The proc will compare nodes based on the node text (or rich content if set).
# The sorting is case insensitive and ascending.
#
# @param left            First element
# @param right           Second element
# @return                An integer less than, equal to, or greater than zero
#                        if the first element is to be considered less than,
#                        equal to, or greater than the second, respectively.
#
proc mm::compare_node_by_text { left right } {

	set strip_html 1
	foreach side {left right} {
		set ${side}_text [node::get --node [set $side] --text]
		if {[set ${side}_text] == {}} {
			set ${side}_text [node::get_rich_content [set $side] $strip_html]
		}
	}

	return [string compare -nocase $left_text $right_text]
}

#
# Returns the DOM object of the mind map.
#
# If no DOM object exists then a new empty mind map is automatically created.
#
# @see                   http://docs.activestate.com/activetcl/8.5/tdom/dom.html
# @return                The DOM object of the mind map
#
proc mm::get_dom {} {

	variable MM

	if {![info exists MM(dom)]} {
		create_empty_mindmap
	}
	return $MM(dom)
}

#
# Retrieves the XML of the mind map.
#
# This XML is the content of what is stored in the FreeMind .mm files.
#
# @return                The XML of the mind map
#
proc mm::get_xml {} {

	variable MM

	if {![info exists MM(dom)]} {
		return
	}

	return [$MM(dom) asXML -indent 0]
}

#
# Internal procedure to retrieve the map element of the DOM tree.
#
# The map element is the enclosing part of the FreeMind XML format. The first,
# and only, child node of the map will be the root node (the central node in a
# mind map).
#
# If no map has been loaded then a new empty one will automatically be created.
#
# @return                The map element of the DOM tree
#
proc mm::_get_map_element {} {
	return [[get_dom] selectNodes {map}]
}

#
# Retrieves the root node of the mind map.
#
# @return                The root node (central node in the mind map)
#
proc mm::get_root_node {} {

	set dom [get_dom]
	set root_node [$dom selectNodes {map/node}]
	if {$root_node == {}} {
		set map_node [_get_map_element]
		set root_node [node::create --text root]
		node::add_subnode $map_node $root_node
	}
	return $root_node
}

#
# Retrieves the next optimal position, i.e. left or right of the centre (root)
# node if one wants an even distribution.
#
# @return                a node position ("left" or "right")
#
proc mm::get_next_even_position {} {

	set r_count 0
	set l_count 0
	foreach subnode [node::get_subnodes [mm::get_root_node]] {
		if {[node::get --node $subnode --position] == {right}} {
			incr r_count
		} else {
			incr l_count
		}
	}
	
	return [expr {$l_count > $r_count ? {right} : {left}}]
}


#
# Load a mind map based on a mind map file.
#
# Only one mind map can be loaded into the system at any one time.
#
# If the file does not exist then an empty mind map is created instead.
#
# @param file            The mind map file (.mm) to load into the system
# @return                1 if the file was loaded, 0 if the given file does
#                        not exist and a new empty mind map has been created
#
proc mm::load { file } {

	variable MM

	set loaded 0
	if {[file exists $file]} {
		set xmlfd [tDOM::xmlOpenFile $file]
		set MM(dom) [dom parse -channel $xmlfd]
		set loaded 1
	} else {
		create_empty_mindmap
	}
	set MM(file) $file
	set MM(dirty) 0
	return $loaded
}

#
# Saves a modified mind map to file.
#
# Only one mind map can be loaded into the system at any one time.
#
# Synopsis:
#    mm::save ?--file <file>? ?--backup?
#
# @param --file <file>   Optional parameter that can be used to specify what
#                        file to store the mind map to, if not provided the
#                        file loaded will be used. If no file was loaded then
#                        this proc will return silently with 0.
# @param --backup        Optional parameter to indicate that the original file
#                        should be backed up before changes are saved. The
#                        backup file name will be appended with .bak.<seconds>
#                        where the latter is the number of seconds since epoc.
#                        This can be used to determine at what time and date
#                        the backup was made.
# @return                If the file was saved successfully then 1 is returned,
#                        otherwise 0 is returned
#
proc mm::save args {

	variable MM

	if {[set idx [lsearch -regexp $args {^-+file$}]] > -1} {
		set MM(file) [lindex $args [incr idx]]
	}

	if {![info exists MM(file)] || $MM(file) == {}} {
		return 0
	}

	if {!$MM(dirty)} {
		return 0
	}

	if {[lsearch -regexp $args {^-+backup$}] > -1} {
		file rename $MM(file) $MM(file).bak.[clock seconds]
	}

	set fp [open $MM(file) w]
	puts $fp [get_xml]
	close $fp
	set MM(dirty) 0

	return 1
}

#
# Creates an empty mind map with no root node.
# 
# @return                The map element of the new empty mind map
#
proc mm::create_empty_mindmap {} {

	variable MM

	set dom [dom createDocument map]
	set map [$dom childNodes]

	$map setAttribute version 0.9.0
	$map appendChild [$dom createComment { To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net }]

	node::unselect

	set MM(dom) $dom
	set MM(dirty) 1

	return $map
}
