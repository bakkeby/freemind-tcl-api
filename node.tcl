#
# Copyright (c) 2014, Stein Gunnar Bakkeby, https://bitbucket.org/bakkeby/freemind-tcl-api
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, are permitted
# provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions
#    and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of
#    conditions and the following disclaimer in the documentation and/or other materials provided
#    with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
# FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
# WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
# 

package require tdom

package provide freemind_api 1.0

namespace eval node {

	# Allows for a node to be selected, see node::select
	::set NODE(selected_node) {}
	
	# Creates the rich_content_appender command that allows HTML to be
	# added to the DOM tree in verbatim.
	dom createNodeCmd textNode rich_content_appender

	# These are not currently being used, but may be used for reference.
	::set FORMAT(styles)      {bubble fork}
	::set FORMAT(edge_styles) {linear bezier sharp_linear sharp_bezier}
	::set FORMAT(edge_width)  {parent thin 1 2 4 8}
	::set FORMAT(icons)       {
		attach back bell bookmark broken-line button_cancel button_ok
		calendar clanbomber clock closed desktop_new down edit family
		fema female1 female2 flag flag-black flag-blue flag-green flag-orange
		flag-pink flag-yellow folder forward freemind_butterfly
		full-0 full-1 full-2 full-3 full-4 full-5 full-6 full-7 full-8 full-9
		go gohome group help hourglass idea info kaddressbook kmail knotify
		korn ksmiletris launch licq list Mail male1 male2 messagebox_warning
		password pencil penguin prepare smiley-angry smiley-neutral smiley-oh
		smily_bad stop stop-sign up wizard xmag yes
	}

	# Parameters
	#            name                        multi   element      attribute         validator    default 
	::set PARAMS(--bg_color)                {0       {}           BACKGROUND_COLOR  color        {}                       }
	::set PARAMS(--color)                   {0       {}           COLOR             color        {}                       }
	::set PARAMS(--created)                 {0       {}           CREATED           datetime     {[clock milliseconds]}   }
	::set PARAMS(--folded)                  {0       {}           FOLDED            true         {true}                   }
	::set PARAMS(--id)                      {0       {}           ID                {}           {ID_[clock clicks]}      }
	::set PARAMS(--link)                    {0       {}           LINK              link         {}                       }
	::set PARAMS(--modified)                {0       {}           MODIFIED          datetime     {[clock milliseconds]}   }
	::set PARAMS(--position)                {0       {}           POSITION          leftright    {}                       }
	::set PARAMS(--style)                   {0       {}           STYLE             {}           {}                       }
	::set PARAMS(--text)                    {0       {}           TEXT              {}           {}                       }
	::set PARAMS(--attribute)               {1       attribute    NAME              {}           {}                       }
	::set PARAMS(--bold)                    {0       font         BOLD              true         {true}                   }
	::set PARAMS(--italic)                  {0       font         ITALIC            true         {true}                   }
	::set PARAMS(--font)                    {0       font         NAME              {}           {SansSerif}              }
	::set PARAMS(--size)                    {0       font         SIZE              size         {12}                     }
	::set PARAMS(--edge_color)              {0       edge         COLOR             color        {}                       }
	::set PARAMS(--edge_width)              {0       edge         WIDTH             {}           {}                       }
	::set PARAMS(--edge_style)              {0       edge         STYLE             {}           {}                       }
	::set PARAMS(--cloud)                   {0       cloud        {}                {}           {true}                   }
	::set PARAMS(--arrow_destination)       {0       arrowlink    DESTINATION       node         {}                       }
	::set PARAMS(--arrow_endarrow)          {0       arrowlink    ENDARROW          {}           {Default}                }
	::set PARAMS(--arrow_endinclination)    {0       arrowlink    ENDINCLINATION    {}           {}                       }
	::set PARAMS(--arrow_id)                {0       arrowlink    ID                {}           {Arrow_ID_[clock clicks]}}
	::set PARAMS(--arrow_startarrow)        {0       arrowlink    STARTARROW        {}           {}                       }
	::set PARAMS(--arrow_startinclination)  {0       arrowlink    STARTINCLINATION  {}           {}                       }
	::set PARAMS(--hook)                    {1       hook         NAME              {}           {}                       }
	::set PARAMS(--icon)                    {1       icon         BUILTIN           {}           {}                       }
	::set PARAMS(--rich_content)            {0       rich_content TYPE              {}           {NODE}                   }
	::set PARAMS(--parent)                  {1       subnode      {}                dom          {}                       }
	::set PARAMS(--subnode)                 {1       subnode      {}                dom          {}                       }
	::set PARAMS(--subnodes)                {1       subnodes     {}                dom          {}                       }
	::set PARAMS(--select)                  {1       {}           {}                node         {}                       }

	# This creates procs on the form node::text { node value } where
	# the value determines the action:
	# If value is:
	#    - not set then the proc acts as a get
	#    - set to a given value then the proc acts as a set
	#    - set to empty string then proc acts as an unset / remove
	foreach param [array names PARAMS] {
		::set strict 0
		::set arg_prefix {-}
		if {$param in {--parent --subnode --subnodes}} {
			::set strict 1
		}
		proc [string trimleft $param -] { args } [subst -nocommands {

			lassign [_get_node_arg \$args $arg_prefix $strict] node remaining_args

			::set value \$remaining_args
			if {[llength \$remaining_args] == 0} {
				::set remaining_args "\u0"
			}
			switch -- [lindex \$remaining_args 0]  {
			"\u0" {
				return [node::get --node \$node $param]
			}
			{} {
				return [node::remove --node \$node $param]
			}
			default {
				return [node::set --node \$node $param {*}\$value]
			}
			}
		}]
	}

	# Attribute font size width definition
	# This defines the width of each character for each font size for attribute
	# names and values.
	# See proc node::optimise_attribute_width
	::set font_size_map {
		6 {
			1 {: ; { } ' . ,}
			2 {f I i j l r t - ! ] [ ) ( / |}
			3 {a c E e F g h J k L n S s T u v x y Z z 0 1 2 3 4 5 6 7 8 9 $ £ ~ + = _ ^ < >}
			4 {A B b C D d G H K N o P p q R U V X Y # &}
			5 {M m O Q w}
			6 {W \{ \} @}
		}
		12 {
			3 {I i j l : ; { } ' ! . , |}
			4 {] [ ) (}
			5 {f r v y - /}
			6 {a F J k L S s t x z _}
			7 {A b c d E e g h n o P p q T u V X Y Z 0 1 2 3 4 5 6 7 8 9 $ £ ~ + = ^ < >}
			8 {B C G H K R U # &}
			9 {D N O Q w}
			11 {M m W @}
			13 {\{ \}}
		}
		24 {
			10 {t}
			11 {c s y z}
			12 {J L T v x _}
			13 {a e F k S}
			14 {b d E g h n o p q u Y Z 0 1 2 3 4 5 6 7 8 9 $ £ ~ + = ^ < >}
			15 {A B C K P R X}
			16 {G V # &}
			17 {D H N U}
			18 {O}
			19 {Q w}
			20 {M}
			22 {m}
			23 {W @}
			27 {\{ \}}
			6 {I i j : ; { } ' . ,}
			7 {l ! |}
			8 {- ] [ ) (}
			9 {f r /}
		}
	}

	foreach {font_size letter_map} $font_size_map {
		foreach {width letter_list} $letter_map {
			foreach letter $letter_list {
				::set FONT_SIZE($font_size,$letter) $width
			}
		}
	}
}

#
# Internal procedure to create a new empty node.
#
# The new node will have an ID and have the created and modified
# time stamps set based on the current time.
#
# @see                   node::create
# @return                The newly created node
#
proc node::_create_new_node {} {

	variable PARAMS

	::set node [[mm::get_dom] createElement node]

	foreach param {created id modified} {
		lassign $PARAMS(--$param) - - attribute - default
		$node setAttribute $attribute [subst $default]
	}
	return $node
}

#
# Creates a new node an sets specific attributes based on the passed
# in arguments.
#
# The node ID and timestamps will be auto generated unless overridden.
#
# Synopsis:
#    node::create ?--param? ... ?
#
# @see                            node::set for what parameters are supported
# @return                         The newly created node
#
proc node::create args {

	lassign [mm::get_args $args {--text --parent --if_not_exists} {{} {} 0}] text parent notexists
	if {$parent != {} && $notexists == 1} {
		# There could potentially be more than one match, so just return
		# the first one if that's the case
		::set node [lindex [mm::node_path_search $text $parent {}] 0]
		if {$node != {}} {
			return $node
		}
	}

	::set node [_create_new_node]
	node::set --node $node {*}$args
	return $node
}

#
# Sets specific attributes for a node based on the passed in arguments.
#
# Note that some parameters can be added multiple times, such as attribute,
# hook and icon.
#
# Synopsis:
#    node::set --node <node> ?--param? ... ?
#
# Example usage:
#    node::set --node $node --bold --icon idea --color #aa33cc
#
# @param --node <node>            The node being updated, if no node is
#                                 provided then the selected one will be used.
# @param --select                 Mark the given node to be selected for
#                                 further updates
# @param --bg_color <rgb>         Specifies the background color of the node,
#                                 e.g. #ffff00
# @param --color <rgb>            Specifies the foreground (i.e. font) color,
#                                 e.g. #aa33cc
# @param --created <ms>           Sets the created timestamp, in milliseconds
#                                 since epoc
# @param --modified <ms>          Sets when the node was last modified, in
#                                 milliseconds since epoc
# @param --folded                 Fold the given node, also see node::fold and
#                                 node::unfold
# @param --id <ID>                Specify the node ID
# @param --link <ID>              Links the node to another node
# @param --link <URL>             Sets an external link, note that a node can
#                                 only contain one link
# @param --position <pos>         Node position, i.e. left or right of centre
#                                 node. Only relevant for direct children of
#                                 the root node.
# @param --style <style>          Assign a style to the node, e.g. bubble or
#                                 fork.
# @param --text <text>            Sets the (plain) text/name of the node
# @param --attribute <key> <val>  Add custom node attributes in key value pairs
#                                 This can be passed multiple times to add more
#                                 attributes to the node
# @param --bold                   Makes node text/name bold
# @param --italic                 Makes node text/name italic
# @param --font <font>            Specifies the font used for the node text
# @param --size <size>            Specifies the font size of the node
# @param --edge_style <style>     The edge here refers to the line between the
#                                 current node and it's parent node. If no edge
#                                 style is defined then the style is inherited
#                                 from the parent node. Possible styles include:
#                                 linear, bezier, sharp_linear and sharp_bezier
# @param --edge_color <rgb>       This defines the RGB color of the edge line
# @param --edge_width <val>       This defines the width of the edge line,
#                                 typical values include:
#                                 1, 2, 4, 8, "parent" and "thin" 
# @param --hook <path>            Adds a hook for a plugin to control the style
#                                 of the node. One example is the blinking node
#                                 hook.
# @param --icon <name>            Sets an icon for the node, examples include:
#                                 "yes", "stop" and "idea". See FORMAT(icons) in
#                                 node.tcl for a more complete list.
# @param --rich_content <html>    Glorify the node content with rich text, i.e.
#                                 plain HTML that is rendered allowing further
#                                 control of layout and look-and-feel.
# @param --parent <node>          Set current node as child of the given parent
#                                 node
# @param --if_not_exists          create the node if such a path does not already
#                                 exist, this is only relevant when creating
#                                 nodes and if the --parent node is also supplied
# @param --subnode <node>         Add a node as a child to the current node
# @param --subnodes <node> ...    Add multiple child nodes to the current node
# @see                            node::select
# @return                         The same node that was passed in
# 
proc node::set { args } {

	variable PARAMS

	lassign [_get_node_arg $args] node remaining_args
	::set _split_args [mm::_split_args $remaining_args 1]

	foreach { param values } $_split_args {

		if {![info exists PARAMS(--$param)]} {
			continue
		}

		lassign $PARAMS(--$param) multi element attribute validator default

		switch -- $param {
		attribute {
			if {$values == {}} {
				::set values {* *}
			}
			foreach {key value} $values {
				node::add_attribute $node $key $value
			}
		}
		rich_content {
			node::set_rich_content --node $node --content [join $values]
		}
		select {
			node::select $node
		}
		parent {
			node::add_subnode $values $node
		}
		subnode -
		subnodes {
			# Double foreach loop, redundant, but allows for these
			# two parameter forms:
			#    node::set --node $n --subnodes $sn1 $sn2 $sn3
			#    node::set --node $n --subnodes [list $sn1 $sn2 $sn3]
			foreach value $values {
				foreach val $value {
					node::add_subnode $node $val
				}
			}
		}
		default {
			if {$values == {} && $default != {}} {
				::set values [list [subst $default]]
			}
			node::_set $node $element $multi $attribute {*}$values
		}
		}
	}

	return $node
}

#
# Internal procedure to set generic values based on the PARAMS
# namespace variable.
#
# @param node            The node to set the value for
# @param element         If the value is part of a separate XML element such
#                        as edge, attribute, or font.
# @param attribute       The attribute being set
# @param args            The specific attribute value(s) being set, if not
#                        passed in then a default value will be used (if
#                        applicable)
# @see                   node::set
#
proc node::_set { node element multi attribute args } {

	variable PARAMS

	if {$args == {}} {
		::set args {*}
	}

	foreach value $args {
		if {$element == {}} {
			if {$attribute != {}} {
				# If the link looks like a DOM node, try to fetch the ID of said node and
				# add that as a reference instead.
				if {$attribute == {LINK} && [string match {domNode0x*} $value]} {
					::set value #[node::get --node $value --id]
				}
				$node setAttribute $attribute $value
			}
			continue
		}

		if {$multi} {
			::set subnode [[mm::get_dom] createElement $element]
			node::add_subnode $node $subnode
		} else {
			::set subnode [lindex [node::_get_element $node $element 1] 0]
		}
		if {$attribute != {} && $value != {*}} {
			$subnode setAttribute $attribute $value
		}
	}
}

#
# Alias for node::remove.
#
# Synopsis:
#    node::unset --node <node> ?--param? ... ?
#
# @see                   node::remove
#
proc node::unset { args } {
	return [node::remove {*}$args]
}


#
# Removes specific attributes for a node based on the passed in arguments.
#
# Note that for most parameters a value can be passed in to indicate that the
# attribute should only be removed if the value matches the given value.
#
# Synopsis:
#    node::remove --node <node> ?--param? ... ?
#
# E.g.
#    node::remove --node $node --color #aa33cc
#
# This would only remove the color attribute of the value if the currently set
# color is in fact #aa33cc.
#
# @param --node <node>            The node being updated
# @param --select                 Mark the given node to be selected for
#                                 further updates
# @param --bg_color               Removes the background color of the node
# @param --color                  Removes foreground color of the node
# @param --created                Removes the created timestamp
# @param --modified               Removes the last modified timestamp
# @param --folded                 Removes the folded attribute of the node,
#                                 this essentially unfolds the node, also see
#                                 node::unfold
# @param --id                     The ID of the node
# @param --link                   Remove the link set for this node
# @param --position               Node position, i.e. left or right of centre
#                                 node
# @param --style                  Remove the style from the node
# @param --text                   Removes the name/text of the node
# @param --attribute <key> <val>  Removes custom node attributes in key value
#                                 pairs.  This can be passed multiple times to
#                                 remove more attributes from the node. If the
#                                 value is passed in as * then all attributes
#                                 with the given key are removed. If the key
#                                 is passed in as * then all attributes with
#                                 the given value will be removed. If no
#                                 parameters are passed in then all attributes
#                                 will be removed from the node.
# @param --bold                   The node text should no longer be in bold
# @param --italic                 The node text should no longer be in italic
# @param --font                   Removes the font from the node
# @param --size                   Removes the font size from the node
# @param --edge_style             Removes the edge style from the node
# @param --edge_color             Removes the edge color from the node
# @param --edge_width             Removes the edge width 
# @param --hook                   Removes a hook from the node
# @param --icon <?name?>          Removes an icon from the node. If the name of
#                                 the icon is provided then just the matching
#                                 one will be removed. If no name is passed in
#                                 then all icons will be removed.
# @param --rich_content           Remove the rich content from the node
# @param --subnode <node> <text>  Remove a child node matching the given text.
#                                 If there is any need to get the references to
#                                 the removed child nodes then using node::get
#                                 first or using the node::remove_subnode proc
#                                 directly would be recommended.
# @param --subnodes <node> ...    Removes all child nodes of the current node
# @see                            node::set for a better explanation of what
#                                 all these parameters do
# @return                         The same node that was passed in
# 
proc node::remove { args } {

	variable PARAMS

	lassign [_get_node_arg $args] node remaining_args
	::set _split_args [mm::_split_args $remaining_args 1]

	foreach { param values } $_split_args {

		if {![info exists PARAMS(--$param)]} {
			continue
		}

		lassign $PARAMS(--$param) multi element attribute validator default

		switch -- $param {
		attribute {
			if {$values == {}} {
				::set values {* *}
			}
			foreach {key value} $values {
				node::remove_attribute $node $key $value
			}
		}
		select {
			node::select $node
		}
		subnode {
			foreach value $values {
				node::remove_subnode $node $value
			}
		}
		subnodes {
			node::remove_subnodes --node $node
		}
		rich_content {
			node::remove_rich_content $node
		}
		default {
			if {$values == {}} {
				::set values {*}
			}
			foreach value $values {
				node::_remove $node $element $attribute $value
			}
		}
		}
	}

	return $node
}

#
# Internal procedure to remove generic values based on the PARAMS
# namespace variable.
#
# @param node            The node to remove the value from
# @param element         If the value is part of a separate XML element such
#                        as edge, attribute, or font.
# @param attribute       The attribute being removed, can be set to * to
#                        indicate that all attributes with a given value should
#                        be removed.
# @param value           The specific attribute value being removed, defaults
#                        to * to indicate all values for the attribute are to
#                        be removed.
# @see                   node::remove
#
proc node::_remove { node element attribute { value * } } {

	if {[lsearch [list {} "\u0"] $value] > -1} {
		::set value {*}
	}

	if {$element == {}} {
		if {[$node hasAttribute $attribute] && [string match -nocase $value [$node getAttribute $attribute]]} {
			$node removeAttribute $attribute
			mm::mark_dirty
		}
		return
	}

	foreach element [node::_get_element $node $element] {

		if {[$element hasAttribute $attribute] && [string match -nocase $value [$element getAttribute $attribute]]} {
			$element removeAttribute $attribute
			mm::mark_dirty
		}
		if {[llength [$element attributes]] == 0} {
			$node removeChild $element
			mm::mark_dirty
		}
	}
}

#
# Deletes a node from the mind map.
#
# Note that this action will be irreversible.
#
# Synopsis:
#    node:delete node ?node? ... ?
#
# @param node            One or more nodes to delete, if not provided then the
#                        selected node will be deleted.
#
proc node::delete { args } {

	if {[llength $args] == 0 || $args == "\u0"} {
		set args [node::selected]
	}

	foreach nodes $args {
		# This is essentially a redundant for-loop, but allows for
		# use cases like:
		#    node::delete $node1 $node2 $node3
		#    node::delete [list $node1 $node2 $node3]
		#    node::delete [node::remove_subnodes $node]
		#
		foreach node $nodes {
			if {![string match dom* $node]} {
				error "wrong # args: should be \"[lindex [info level [expr {[info level] - 1}]] 0] node\""
			}

			$node delete
			mm::mark_dirty
		}
	}
}

#
# Retrieves specific attributes for a node based on the passed in arguments.
#
# In general if any of the request parameters return an empty string then that
# indicates that the default within FreeMind will be used.
#
# Note that some fields may appear multiple times for the node, such as
# attributes, hooks and icons. These values will be returned as a list within
# the returned list.
#
# Synopsis:
#    node::get --node <node> ?--param? ... ?
#
# Example usage:
#    set res [node::get --node $node --created --id --text]
#    lassign $res created id text
#
# @param --node <node>            The node containing the data
# @param --select                 Mark the given node to be selected for
#                                 further updates
# @param --bg_color               Retrieves the background color of the node
# @param --color                  Retrieves foreground color of the node
# @param --created                Retrieves the created timestamp
# @param --modified               Retrieves the last modified timestamp
# @param --folded                 Retrieves the folded attribute of the node,
#                                 returns true if the node is folded, false or
#                                 empty string if not
# @param --id                     The ID of the node
# @param --link                   Retrieve the link set for this node, if the
#                                 node is linked to another node then the ID of
#                                 the linked node will be returned
# @param --position               Node position, i.e. left or right of centre
#                                 node. 
# @param --style                  Retrieve the style of the node
# @param --text                   Retrieves the name/text of the node
# @param --attribute <key> <val>  Retrieves custom node attributes in key value
#                                 pairs, also see node::get_attribute
# @param --bold                   Returns true if node is bold, false or empty
#                                 string otherwise
# @param --italic                 Returns true if node is in italic, false or
#                                 empty string otherwise
# @param --font                   Retrieves the font of the node, returns empty
#                                 string if default (the default is SansSerif)
# @param --size                   Retrieves the font size of the node, or empty
#                                 string if not set (the default is 12)
# @param --edge_style             Retrieves the edge style of the node
# @param --edge_color             Retrieves the edge color of the node
# @param --edge_width             Retrieves the edge width 
# @param --hook                   Retrieves the hooks set for the node
# @param --icon                   Retrieves the icons set for the node
# @param --rich_content           Retrieve the rich content from the node, also
#                                 see node::get_rich_content
# @param --parent                 Returns the parent node
# @param --subnode <node> <text>  Retrieves a child node matching the given text
# @param --subnodes <node> ...    Retrieves all child nodes from the current node
# @see                            node::set for a better explanation of what
#                                 all these parameters do
# @return                         A list of results based on the parameters
#                                 passed in to the proc
# 
proc node::get { args } {

	variable PARAMS

	lassign [_get_node_arg $args] node remaining_args
	::set _split_args [mm::_split_args $remaining_args 1]
	::set output {}

	foreach { param values } $_split_args {

		if {![info exists PARAMS(--$param)]} {
			continue
		}

		lassign $PARAMS(--$param) multi element attribute validator default

		switch -- $element {
		{} {
			lappend output [$node getAttribute $attribute {}]
		}
		attribute {
			lappend output [node::get_attribute $node {*}$values]
		}
		rich_content {
			lappend output [node::get_rich_content $node]
		}
		select {
			node::select $node
		}
		parent {
			lappend output [$node parentNode]
		}
		subnode {
			set subnodes {}
			foreach value $values {
				lappend subnodes [node::get_subnode $node $value]
			}
			lappend output $subnodes
		}
		subnodes {
			lappend output [node::get_subnodes $node]
		}
		default {
			::set subnode_vals {}
			if {$values == {}} {
				::set values {*}
			}
			foreach subnode [node::_get_element $node $element] {
				foreach value $values {
					if {$value == {*} || [string match -nocase $value [$subnode getAttribute $attribute {}]]} {
						lappend subnode_vals [$subnode getAttribute $attribute $default]
					}
				}
			}
			if {[llength $subnode_vals] == 1} {
				lappend output {*}$subnode_vals
			} else {
				lappend output $subnode_vals
			}
		}
		}
	}

	if {[llength $output] == 1} {
		return {*}$output
	}
	return $output
}

#
# Add an attribute for a given node.
#
# An "attribute" here refers to custom key value pairs of information that can
# be added to nodes in the FreeMind application.
#
# This should not be confused with DOM attributes.
#
# Synopsis:
#    node::add_attribute node attribute value
#
# @param node            The node to add the attribute for
# @param attribute       The name of the attribute to set
# @param value           The value to set the attribute to
#
proc node::add_attribute { node attribute value } {
	::set dom [mm::get_dom]
	::set attrib_node [$dom createElement attribute]
	$attrib_node setAttribute NAME $attribute
	$attrib_node setAttribute VALUE $value
	$node appendChild $attrib_node
	mm::mark_dirty

}

#
# Removes an attribute from a given node.
#
# Synopsis:
#    node::remove_attribute node ?attribute? ?value?
#
# @param node            The node to remove the attribute from
# @param attribute       The name of the attribute to set, if not set then all
#                        attributes matching the given value will be removed.
#                        If both the attribute and value are not set then all
#                        attributes will be removed from the given node.
# @param value           The attribute value to remove, if not set then all
#                        values of the given attribute will be removed
# @see                   add_attribute for further details
#
proc node::remove_attribute { node { attribute {*} } { value {*} } } {
	::set attrib_nodes [node::_get_element $node attribute]
	foreach param {attribute value} {
		if {[lsearch [list {} "\u0"] [::set $param]] > -1} {
			::set $param {*}
		}
	}

	foreach attrib_node $attrib_nodes {
		if {(![$attrib_node hasAttribute NAME ] || [string match -nocase $attribute [$attrib_node getAttribute NAME ]]) &&
		    (![$attrib_node hasAttribute VALUE] || [string match -nocase $value     [$attrib_node getAttribute VALUE]])} {
			$node removeChild $attrib_node
		}
	}
	mm::mark_dirty
}

#
# Retrieves an attribute from a given node.
#
# Synopsis:
#    node::get_attribute node ?attribute? ?attribute? ...
#
# @param node            The node to get the attribute from
# @param attribute       The name of the attribute to get, if not set then all
#                        attributes will be returned. Multiple attribute names
#                        may be retrieved at once.
# @see                   add_attribute for further details
#
proc node::get_attribute { node args } {
	::set output {}
	if {[llength $args] == 0} {
		lappend args {*}
	}
	foreach attrib_node [$node selectNodes "attribute"] {
		foreach value $args {
			::set attribute [$attrib_node getAttribute NAME]
			if {[string match -nocase $value $attribute]} {
				lappend output $attribute [$attrib_node getAttribute VALUE]
				break
			}
		}
	}
	return $output
}

#
# Procedure to simulate the "Optimal Width" functionality when right clicking
# attributes within FreeMind.
#
# The optimal width is determined by the graphical width a text takes up based
# on the number of pixels needed to display it.
#
# This procedure tries to calculate and set this optimal width based on
# gathered stats, but may not provide 100% accurate sizes.
#
# Synopsis:
#    node::optimise_attribute_width --node <node>
#
# @param node            The node to optimise the attribute width for
#
proc node::optimise_attribute_width { args } {

	lassign [_get_node_arg $args {-} 0] node remaining_args

	::set max_name_length 0
	::set max_value_length 0
	foreach attribute [node::_get_element $node attribute] {
		::set name_length  [_calc_attribute_width [$attribute getAttribute NAME]]
		::set value_length [_calc_attribute_width [$attribute getAttribute VALUE]]
		if {$name_length > $max_name_length} {
			::set max_name_length $name_length
		}

		if {$value_length > $max_value_length} {
			::set max_value_length $value_length
		}
	}
	if {$max_name_length > 0 || $max_value_length > 0} {
		::set attrib_layout_node [node::_get_element $node attribute_layout]
		if {$attrib_layout_node == {}} {
			::set dom [mm::get_dom]
			::set attrib_layout_node [$dom createElement attribute_layout]
			node::add_subnode $node $attrib_layout_node
			# I was just thinking, should we have a flag to pass to _get_element
			# to automatically create the element if it doesn't already exist
		}
		$attrib_layout_node setAttribute NAME_WIDTH $max_name_length VALUE_WIDTH $max_value_length
	}
}

#
# Internal procedure responsible for estimating the required display width in
# pixels of an attribute text.
#
# @param attribute       The text to estimate required pixel width for
# @return                The estimated width of the attribute text
#
proc node::_calc_attribute_width { attribute } {

	variable FONT_SIZE

	::set font_size 12; # todo, get from attribute_registry default to 12
	::set total_width 4
	foreach letter [split $attribute {}] {
		if {[info exists FONT_SIZE($font_size,$letter)]} {
			::set total_width [expr {$total_width + $FONT_SIZE($font_size,$letter)}]
		} else {
			::set total_width [expr {$total_width + $font_size / 2.0 + $font_size / 12}]; # est. default
		}
	}
	return [format %.0f $total_width]
} 

#
# Internal procedure to get hold of a node's element attributes, such as
# font, edge, icon, etc.
#
# @param node            The node being referenced
# @param element         The element being looked up, edge, icon, font, etc.
# @param auto_create     A value of 1 indicates that the element should be
#                        created if it does not currently exist, defaults sto 0
# @return                The element DOM node, if found
# 
proc node::_get_element { node element { auto_create 0 } } {

	variable PARAMS

	::set element_node [$node selectNodes $element]

	if {[llength $element_node] == 0 && $auto_create == 1} {

		::set element_node [[mm::get_dom] createElement $element]
		
		if {$element == {font}} {
			foreach param {font size} {
				lassign $PARAMS(--$param) - - attribute - default
				if {$default != {}} {
					$element_node setAttribute $attribute [subst $default]
				}
			}
		}
		
		node::add_subnode $node $element_node
	}
	return $element_node
}

#
# Select a node for multiple updates.
#
# Synopsis:
#    node::select --node <node>
#    node::select <node>
#
# A selected node can be used with the following short-hand alternatives for
# the get, set and remove procs:
#    node::bg_color
#    node::color
#    node::created
#    node::folded
#    node::id
#    node::link
#    node::modified
#    node::position
#    node::style
#    node::text
#    node::attribute
#    node::bold
#    node::italic
#    node::font
#    node::size
#    node::edge_color
#    node::edge_width
#    node::edge_style
#    node::cloud
#    node::hook
#    node::icon
#    node::rich_content
#
# Example usage:
#    node::select $node
#    set old_text [node::text]; # retrieves the text of the node
#    node::text "new text";     # sets the new text for the node
#    node::text {};             # removes the text from the node
#
# @param node            The node being selected
# @see                   node::set for what each of these parameters do
#
proc node::select { args } {

	variable NODE

	::set NODE(selected_node) {}
	lassign [_get_node_arg $args {-} 0] node remaining_args

	::set NODE(selected_node) $node
}

#
# Unselect a node.
#
# This is primarily for test purposes.
#
# @see                   node::select
#
proc node::unselect {} {

	variable NODE

	::set NODE(selected_node) {}
}

#
# Retrieves the currently selected node.
#
# @see                   node::select
# @return                The selected node or empty string if none has been set
#
proc node::selected {} {

	variable NODE

	return $NODE(selected_node)
}

#
# Folds the given node. If no node is passed in then the selected node will be
# used.
#
# Synopsis:
#    node::fold ?--node <node>?
#    node::fold ?<node>?
#
# If the node is a leaf node then it will not be folded.
#
# @param node            The node to fold, if no node is  provided then the
#                        selected node will be used
# @param --node <node>   Same as node param above
#
proc node::fold { args } {

	lassign [_get_node_arg $args {-} 0] node remaining_args

	if {[llength [node::get_subnodes $node]] > 0} {
		node::set --node $node --folded true
	}
}

#
# Unfolds the given node. If no node is passed in then the selected node will
# be used.
#
# Synopsis:
#    node::unfold ?--node <node>?
#    node::unfold ?<node>?
#
# @param node            The node to unfold, if no node is  provided then the
#                        selected node will be used
# @param --node <node>   Same as node param above
#
proc node::unfold { args } {

	lassign [_get_node_arg $args {-} 0] node remaining_args

	node::remove --node $node --folded
}

#
# Retrieves the rich content (i.e. HTML formatted content) of the node, if any.
#
# Synopsis:
#    node::get_rich_content --node <node> ?--strip_html <1|0>?
#
# @param node            The node to retrieve rich content from, if no node is
#                        provided then the selected node will be used
# @param strip_html      Indicates whether to strip the HTML tags from the
#                        content and just return the plain text. Defaults to
#                        returning HTML (0).
# @return                The rich content in HTML format or plain text if the
#                        strip_html parameter has been passed in as 1.
#
proc node::get_rich_content { args } {

	::set strip_html 0
	lassign [_get_node_arg $args {-} 0] node remaining_args

	if {[llength $remaining_args] > 1} {
		::set split_args [mm::_split_args $remaining_args 1]
		foreach {param values} $split_args {
			::set [string tolower $param] [join $values]
		}
	} elseif {$remaining_args != {}} {
		::set strip_html [lindex $remaining_args 0]
	}

	::set output {}
	foreach rich_text_node [node::_get_element $node richcontent] {
		::set subnodes [$rich_text_node childNodes]
		for {::set i 0} {$i < [llength $subnodes]} {incr i} {
			::set subnode [lindex $subnodes $i]
			if {$strip_html} {
				append output [$subnode nodeValue]
				regsub -all {<[^>]+>} $output {} output
			} else {
				append output [$subnode asXML]
			}
		}
	}

	return [string trim $output]
}

#
# Sets the rich content (i.e. HTML formatted content) of the node.
#
# Synopsis:
#    node::set_rich_content --node <domNode> --content <html>
#    node::set_rich_content <domNode> <html>
#
# @param node              The node to set rich content for, if no node is
#                          provided then the selected node will be used
# @param --node <node>     Same as node param above
# @param content           The rich content (i.e. HTML) to set
# @param --content <html>  Same as content param above
#
proc node::set_rich_content { args } {

	lassign [_get_node_arg $args {-} 0] node remaining_args

	if {[llength $remaining_args] > 1} {
		::set split_args [mm::_split_args $remaining_args 1]
		foreach {param values} $split_args {
			::set [string tolower $param] [join $values]
		}
	} elseif {$remaining_args != {}} {
		::set content [join $remaining_args \n]
	}

	if {![info exists content]} {
		error "Usage: [lindex [info level [info level]] 0] --node <domNode> --content <html>"
	}

	::set subnode [node::_get_element $node richcontent]
	if {$subnode == {}} {
		::set subnode [[mm::get_dom] createElement richcontent]
		$subnode setAttribute TYPE NODE
		node::add_subnode $node $subnode
	}
	foreach childNode [$subnode childNodes] {
		$childNode delete
	}

	$subnode appendFromScript {rich_content_appender -disableOutputEscaping $content}
	mm::mark_dirty
}

#
# Removes the rich content (i.e. HTML formatted content) from the node.
#
# Synopsis:
#    node::remove_rich_content ?--node? <domNode>
#
# @param node            The node to remove rich content from, if no node is
#                        provided then the selected node will be used
# @param --node <node>   Same as node param above
#
proc node::remove_rich_content { args } {

	lassign [_get_node_arg $args {-} 0] node remaining_args

	::set subnode [node::_get_element $node richcontent]
	if {$subnode != {}} {
		$node removeChild $subnode
		mm::mark_dirty
	}
}

#
# Add a node as a child to a parent node.
#
# @param node            The parent node
# @param subnode         The node to add as a child node to the parent node
#
proc node::add_subnode { node subnode } {

	# if subnode is already a sub node of node, then ignore
	if {[lsearch [$node childNodes] $subnode] > -1} {
		return
	}

	if {![string match dom* $node] || ![string match dom* $subnode]} {
		error "wrong # args: should be \"[lindex [info level [info level]] 0] node subnode\""
	}

	# verify that node and subnode are dom nodes?
	$node appendChild $subnode
	mm::mark_dirty
}

#
# Retrieves all child nodes that matches the given plain text.
#
# Note that this will not work with nodes that contains rich content.
#
# @param node            The node to retrieve the child node from
# @param text            The name/text of the child node to retrieve
# @return                One or more child nodes if found
#
proc node::get_subnode { node text } {
	# Text containting backslashes and single quotes cannot be used as
	# part of the XPath search string, so using a starts-with solution
	# instead.
	::set search_string $text
	regexp {^([^\\\']+)} $text --> search_string

	return [$node selectNodes "node\[starts-with(@TEXT,'$search_string')\]"]
}

#
# Retrieves a child nodes of the given node.
#
# Synopsis:
#   node::get_subnodes ?--node <node>?
#
# @param node            The node to retrieve the child nodes from
# @return                All child nodes found, or empty string if the node has
#                        no child nodes
#
proc node::get_subnodes { args } {

	lassign [_get_node_arg $args {-} 0] node remaining_args

	return [$node selectNodes {node}]
}

#
# Checks whether a node has child nodes or not.
#
# If the node has no child nodes then it will be a leaf node.
#
# Synopsis:
#   node::get_subnodes ?--node <node>?
#
# @param node            The node to check whether it has child nodes or not
# @return                1 if the node has child nodes, 0 otherwise
#
proc node::has_subnodes { args } {

	lassign [_get_node_arg $args {-} 0] node remaining_args

	return [expr {[llength [node::get_subnodes $node]] > 0}]
}

#
# Removes the child node that matches the given plain text.
#
# @param node            The node to remove the child node(s) from
# @param text            The name/text of the child node to remove, if more
#                        than one node has the same name/text (unlikely) then
#                        all matching nodes will be returned in a list
# @param delete          Delete the detached node from memory, this will be
#                        irreversible
# @see                   node::remove_subnodes for further details
# @return                The child node(s) removed
#
proc node::remove_subnode { node text {delete 0} } {

	::set subnodes {}
	foreach subnode [node::get_subnode $node $text] {
		$node removeChild $subnode
		lappend subnodes $subnode
		mm::mark_dirty
	}

	if {$delete && [llength $subnodes] > 0} {
		node::delete {*}$subnodes
		return
	}

	return $subnodes
}

#
# Removes all child nodes of the given node.
#
# Note that this will dissociate the nodes as children of the parent node, but
# the nodes will not be explicitly deleted from memory. This means that the
# returned nodes can be added (i.e. moved) to another parent node.
#
# The nodes will exist as DOM fragments in memory until the process ends unless
# they are explicitly deleted. If the removed child nodes are no longer needed
# then the delete parameter can be passed or the node::delete proc can be used.
#
# Synopsis:
#    node::remove_subnodes ?--node <node>? ?--delete?
#
# @param --node <node>   The node to remove the child nodes from
# @param --delete        Delete the node from memory, this will be irreversible
# @see                   node::delete
# @see                   node::remove_subnode for details on removing specific
#                        child nodes
# @return                The child nodes removed
#
proc node::remove_subnodes { args } {

	lassign [_get_node_arg $args {-} 0] node remaining_args

	::set delete [expr {[lsearch -regexp $remaining_args {^-+del(ete)?$}] != -1}]

	::set subnodes {}
	foreach subnode [node::get_subnodes $node] {
		$node removeChild $subnode
		lappend subnodes $subnode
		mm::mark_dirty
	}

	if {$delete && [llength $subnodes] > 0} {
		node::delete {*}$subnodes
		return
	}

	return $subnodes
}

#
# Retrieve the linked node, if any. Note that a node can have a link to either
# an internal node, or an external URL.
#
# Synopsis:
#    node::get_linked_node ?--node <node>?
#
# @param node            The node to retrieve the linked node from, if no node
#                        is provided then the selected node will be used.
# @param --node <node>   Same as node param above
# @return                The linked node if found, empty string otherwise
#
proc node::get_linked_node { args } {

	lassign [_get_node_arg $args {-} 0] node remaining_args

	set linked_node {}
	foreach link [node::get --node $node --link] {
		if {[string match {#*} $link]} {
			set linked_node [node_id_search [string range $link 1 end]]
		}
	}

	return $linked_node
}

#
# Internal procedure to retrieve the node argument from a set of parameters.
#
# If no node parameter is found, then the selected node will be used.
#
# If no node parameter is found and no node is selected, then an error will
# be thrown.
#
# @param arg_list        The parameter list to search the node index for, if
#                        node variable is not found then the selected node will
#                        be returned. If no node has been selected then an
#                        error will be thrown.
# @param arg_prefix      Parmeterised arguments are determined by a prefix such
#                        as a hyphen or two, for example --node. Defaults to
#                        hyphen.
# @param strict          Indicates whether a node dom element in the arg_list
#                        strictly needs to be a parameterised argument. That
#                        means --node <domNode> will be allowed and simply
#                        <domNode> will not be. If no node is found in the
#                        arg_list then the selected node will be used. This is
#                        primarily intended for procs that takes one or more
#                        other nodes as parameters.
# @see                   node::select
# @return                A list of two values: The node and the remaining
#                        arguments.
#
proc node::_get_node_arg { arg_list { arg_prefix - } {strict 1} } {

	::set length [llength $arg_list]
	for {::set i 0} {$i < $length} {incr i} {
		::set val [lindex $arg_list $i]
		::set param_found 0

		# if --node parameter, then remove and skip to next
		if {[regexp ^${arg_prefix}+node\$ $val]} {
			::set arg_list [lreplace $arg_list $i $i]
			::set val [lindex $arg_list $i]
			::set param_found 1
		# if another -- parameter, then skip the next value
		} elseif {[regexp ^${arg_prefix}+ $val]} {
			incr i
			continue
		}

		if {[string match {dom*} $val] && (!$strict || $param_found)} {
			::set arg_list [lreplace $arg_list $i $i]
			return [list $val $arg_list]
		}
	}

	::set node [node::selected]
	if {$node == {}} {
		error "Usage: [lindex [info level [expr {[info level] - 1}]] 0] --node <domNode>"
	}

	return [list $node $arg_list]
}
